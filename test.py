#!/usr/bin/env python
"""
Copyright (c) 2012, 2013 TortoiseLabs LLC

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

This software is provided 'as is' and without any warranty, express or
implied. In no event shall the authors be liable for any damages arising
from the use of this software.
"""

import pyxen

s = pyxen.Session()
domlist = s.domain_list()

print "%4s: %-20s %-10s %-10s %-7s %12s" % ("ID", "Name", "Memory", "VCPUs", "State", "CPU Time")

for dom in domlist:
    st = dom.stats()
    print "%04d: %-20s %-10d %-10d %-7s %12.2f" % (dom.domid, st['name'], st['current_mem_kb'] / 1024, st['vcpu_count'], st['state'], st['cputime_sec'])
